# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :frontend,
  namespace: PA.FrontEnd

# Configures the endpoint
config :frontend, PA.FrontEnd.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "5C9bcGZZRksox4i5xU1eZh3tkxLNxRybSljO3bR9rkIVlpTo9jqYWWPYbvcVQYzq",
  render_errors: [view: PA.FrontEnd.ErrorView, accepts: ~w(html json)],
  pubsub: [name: PA.FrontEnd.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :phoenix, :template_engines,
  slim: PhoenixSlime.Engine,
  slime: PhoenixSlime.Engine

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
