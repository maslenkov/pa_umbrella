defmodule PA.FrontEnd.PageController do
  use PA.FrontEnd.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
