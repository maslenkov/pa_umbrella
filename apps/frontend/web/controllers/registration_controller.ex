defmodule PA.FrontEnd.RegistrationController do
  use PA.FrontEnd.Web, :controller

  def create(conn, %{"registration" => registration_params}) do
    case PA.BackEnd.Registrator.register(registration_params) do
      {:ok, registration} ->
        PA.MailMan.send_thanks_for_registration(registration.email)
        conn
        |> put_flash(:info, "Registration created successfully.")
        |> redirect(to: page_path(conn, :index))
      {:error, changeset} ->
        render conn, PA.FrontEnd.PageView, "index.html"
    end
  end
end

