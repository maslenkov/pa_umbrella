defmodule PA.FrontEnd.Router do
  use PA.FrontEnd.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", PA.FrontEnd do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    post "/registration", RegistrationController, :create
  end

  if Mix.env == :dev do
    forward "/sent_emails", PA.MailMan.Router
  end

  # Other scopes may use custom stacks.
  # scope "/api", PA.FrontEnd do
  #   pipe_through :api
  # end
end
