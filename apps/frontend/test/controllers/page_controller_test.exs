defmodule PA.FrontEnd.PageControllerTest do
  use PA.FrontEnd.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "<h2>Registration</h2>"
  end
end
