defmodule PA.FrontEnd.RegistrationControllerTest do
  use PA.FrontEnd.ConnCase

  import Mock

  # check PA.BackEnd.Registrator
  @valid_attrs %{email: "email@example.com"}
  @invalid_attrs %{}

  test_with_mock "calls Registrator.register", %{conn: conn}, PA.BackEnd.Registrator,
    [],
    [register: fn(_params) -> {:error, :_} end] do
      post conn, registration_path(conn, :create), registration: @invalid_attrs
      assert called PA.BackEnd.Registrator.register(:_)
  end

  test_with_mock "calls PA.MailMan", %{conn: conn}, PA.MailMan,
    [],
    [send_thanks_for_registration: fn(_params) -> %{email: @valid_attrs.email} end] do
      post conn, registration_path(conn, :create), registration: @valid_attrs
      assert called PA.MailMan.send_thanks_for_registration(@valid_attrs.email)
  end

  test "redirects when success", %{conn: conn} do
    with_mock PA.BackEnd.Registrator, [register: fn(_params) -> {:ok, @valid_attrs} end] do
      with_mock PA.MailMan, [send_thanks_for_registration: fn(_params) -> {:ok, true} end] do
        conn = post conn, registration_path(conn, :create), registration: @valid_attrs
        assert redirected_to(conn) == page_path(conn, :index)
      end
    end
  end

  test "renders errors when fail", %{conn: conn} do
    with_mock PA.BackEnd.Registrator, [register: fn(_params) -> {:error, false} end] do
      conn = post conn, registration_path(conn, :create), registration: @invalid_attrs
      assert html_response(conn, 200) =~ "<h2>Registration</h2>"
    end
  end
end
