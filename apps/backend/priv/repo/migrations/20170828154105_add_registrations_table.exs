defmodule PA.BackEnd.Repo.Migrations.AddRegistrationsTable do
  use Ecto.Migration

  def change do
    create table("registrations") do
      add :email, :string

      timestamps()
    end
  end
end
