defmodule PA.BackEnd do
  @moduledoc """
  Documentation for PA.BackEnd.
  """

  @doc """
  Hello world.

  ## Examples

      iex> PA.BackEnd.hello
      :world

  """
  def hello do
    :world
  end
end
