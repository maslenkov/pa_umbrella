defmodule PA.BackEnd.Registration do
  use Ecto.Schema
  import Ecto.Changeset

  schema "registrations" do
    field :email
    timestamps()
  end

  def changeset(registration, params \\ %{}) do
    registration |> cast(params, [:email])
    |> validate_required([:email])
    |> validate_format(:email, ~r/@/)
  end
end
