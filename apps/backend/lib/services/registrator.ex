defmodule PA.BackEnd.Registrator do
  def register(params) do
    %PA.BackEnd.Registration{}
    |> PA.BackEnd.Registration.changeset(params)
    |> PA.BackEnd.Repo.insert
  end
end
