# TODO: rename to Services.Registrator or extract models from this app
defmodule PA.BackEnd.RegistratorTest do
  use ExUnit.Case

  alias PA.BackEnd.{Registrator, Registration}

  test "writes into db" do
    assert {:ok, %Registration{email: "email@example.com"}} = Registrator.register(%{email: "email@example.com"})
  end

  test "fails when empty params" do
    assert {:error, %{errors: [email: {"can't be blank", _}]}} = Registrator.register(%{})
  end

  test "fails when incorrect email" do
    assert {:error, %{errors: [email: {"has invalid format", _}]}} = Registrator.register(%{email: "not_an_email"})
  end
end

