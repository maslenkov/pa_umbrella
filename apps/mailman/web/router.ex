# NOTE: router is available only in dev!
defmodule PA.MailMan.Router do
  use Plug.Router

  plug :match
  plug :dispatch

  forward "/", to: Bamboo.EmailPreviewPlug
end
