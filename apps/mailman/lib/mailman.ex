defmodule PA.MailMan do
  def send_thanks_for_registration(email) do
    PA.MailMan.Email.thanks_for_registration(email) |> PA.MailMan.Mailer.deliver_now
  end
end
