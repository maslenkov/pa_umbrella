defmodule PA.MailMan.Email do
  import Bamboo.Email

  def thanks_for_registration(email) do
    new_email
    |> to(email)
    # TODO: from should be from config
    |> from("no-reply@pa-systems.herokuapp.com")
    |> subject("Thank you for your interest")
    |> html_body("You've just registered for our event successfully")
    |> text_body("You've just registered for our event successfully")
  end

  def welcome_email do
    new_email(
      to: "yujiriwc@gmail.com",
      from: "support@pa-systems.herokuapp.com",
      subject: "Welcome to the app.",
      html_body: "<strong>Thanks for joining!</strong>",
      text_body: "Thanks for joining!"
    )

    # # or pipe using Bamboo.Email functions
    # new_email
    # |> to("foo@example.com")
    # |> from("me@example.com")
    # |> subject("Welcome!!!")
    # |> html_body("<strong>Welcome</strong>")
    # |> text_body("welcome")
  end
end
